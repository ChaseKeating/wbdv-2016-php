<html>
<head>
</head>
<body>
  <header>Header</header>
  <?php if(!empty($errors)) { ?>
    <div class="errors">
      <?php echo join(',', $errors) ?>
    </div>
  <?php } ?>
  <?php echo $content ?>
  <footer>Footer</footer>
</body>
</html>
