<?php

function insertTemplateIntoLayout($templateName, $errors = '')
{
  ob_start();
  require $templateName;
  $content = ob_get_clean();

  echo $content;
  // require 'templates/layout.php';
}
