<?php

function arrayDoubler($input, $factor)
{
  $output = [];
  foreach ($input as $value) {
    $output[] = $value * $factor;
  }

  return $output;
}

function listAddresses($input)
{
  $output = [];
  foreach ($input as $key => $value) {
    $output[] = "$key lives at: $value";
  }

  return $output;
}
