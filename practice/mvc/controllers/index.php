<?php

require_once 'functions/tweets.php';

if( !empty($_POST) ) {
  storeTweet($_POST['tweet'], 1);
}
$tweets = getTweets();

require 'templates/content.php';
