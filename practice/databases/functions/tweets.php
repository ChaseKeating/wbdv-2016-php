<?php

function getDbConnection() {
  $host = 'localhost';
  $user = 'me';
  $pass = '1234';
  $dbName = 'sait';

  $db = new mysqli(
    $host,
    $user,
    $pass,
    $dbName
  );

  return $db;
}

function getTweets() {

  $db = getDbConnection();
  $result = $db->query('SELECT * FROM tweets');

  // $tweets = [];
  // while ($row = $result->fetch_assoc()) {
  //   $tweets[] = $row;
  // }

  $tweets = $result->fetch_all(MYSQLI_ASSOC);

  return $tweets;
}

function resetDatabase()
{
    $db = getDbConnection();

    $db->query('DROP TABLE IF EXISTS tweets');

    $sql = "CREATE TABLE IF NOT EXISTS `tweets` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `message` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `timestamp` date NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB;";

    $db->query($sql);
    $db->close();
}

function storeTweet($message, $user_id)
{
  $db = getDbConnection();

  $dateStamp = date('Y-m-d');

  $message = $db->real_escape_string($message);

  // $db->query("
  //   INSERT INTO `sait`.`tweets`
  //   (`id`, `message`, `user_id`, `timestamp`)
  //   VALUES (NULL, '$message', '$user_id', '$dateStamp')
  // ");

  $statement = $db->prepare("
    INSERT INTO `sait`.`tweets`
    (`message`, `user_id`, `timestamp`)
    VALUES (?, ?, ?)
  ");
  $statement->bind_param("sis", $message, $user_id, $dateStamp);
  $statement->execute();
  var_dump($db->error);
}
